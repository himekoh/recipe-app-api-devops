terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-tfstate-20220705"
    key            = "recipe-app.tfstate"
    region         = "ap-northeast-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "ap-northeast-1"
  version = "~> 2.50.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}
